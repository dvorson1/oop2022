package cz.cvut.od.dp.decorator;

public abstract class CoffeeDecorator implements Coffee {

    private final Coffee coffee;

    public CoffeeDecorator(Coffee coffee) {
        this.coffee = coffee;
    }

    public void prepareCoffee() {
        this.coffee.prepareCoffee();
    }

}
