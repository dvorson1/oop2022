package cz.cvut.od.dp.command;

public class SayHiToJohnOnlyCommand implements GenericCommand {

    private final String name;

    public SayHiToJohnOnlyCommand(String name) {
        this.name = name;
    }

    @Override
    public void execute() {
        if (!"John".equals(this.name)) {
            return;
        }
        System.out.println("Hi " + name);
    }
}
