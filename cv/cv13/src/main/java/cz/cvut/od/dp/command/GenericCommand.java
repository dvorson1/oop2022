package cz.cvut.od.dp.command;

public interface GenericCommand {
    void execute();
}
