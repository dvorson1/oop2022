package cz.cvut.od.util;

import java.util.Iterator;
import java.util.List;

public class OwnIterator<TYPE> implements Iterator<TYPE> {

    private List<TYPE> data;
    private int currentIndex = 0;

    public OwnIterator(List<TYPE> data) {
        this.data = data;
    }

    @Override
    public boolean hasNext() {
        return currentIndex < data.size();
    }

    @Override
    public TYPE next() {
        return data.get(currentIndex++);
    }
}
