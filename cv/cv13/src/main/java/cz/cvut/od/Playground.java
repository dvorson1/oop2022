package cz.cvut.od;

import cz.cvut.od.dp.command.GenericCommand;
import cz.cvut.od.dp.command.PrintHelloNameCommand;
import cz.cvut.od.dp.command.SayHiToJohnOnlyCommand;
import cz.cvut.od.dp.decorator.BasicCoffee;
import cz.cvut.od.dp.decorator.Coffee;
import cz.cvut.od.dp.decorator.CoffeeWithMilk;
import cz.cvut.od.dp.decorator.SweetCoffee;
import cz.cvut.od.util.OwnList;

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import static java.util.Arrays.asList;

public class Playground {

    public void play() {
        iteratorList();
        commands();
        decorator();
    }

    public void decorator() {
        Coffee coffee = new SweetCoffee(new CoffeeWithMilk(new BasicCoffee()));
        coffee.prepareCoffee();
    }

    public void commands() {
        GenericCommand a = new PrintHelloNameCommand("A");
        GenericCommand b = new PrintHelloNameCommand("C");
        GenericCommand c = new SayHiToJohnOnlyCommand("John");
        GenericCommand d = new SayHiToJohnOnlyCommand("David");
        List<GenericCommand> commands = Arrays.asList(a, b, c, d);

        for (GenericCommand command : commands) {
            command.execute();
        }
    }

    public void iteratorList() {
        List<String> list = asList("A", "B", "C");

        for (int i = 0; i < list.size(); i++) {
            String item = list.get(i);
            System.out.println(item);
        }

        for (String item : list) {
            System.out.println(item);
        }

        Iterator<String> iterator = list.iterator();
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        System.out.println(iterator.next());
        //System.out.println(iterator.next());

        int i = 0;
        while (i < list.size()) {
            System.out.println(list.get(i));
            i++;
        }

        Iterator<String> it = list.iterator();
        while (it.hasNext()) {
            System.out.println(it.next());
        }

        System.out.println("-----");
        OwnList<String> myList = new OwnList<>();
        myList.add("A");
        myList.add("B");
        myList.add("C");

        Iterator<String> it2 = myList.iterator();
        while (it2.hasNext()) {
            System.out.println(it2.next());
        }
    }
}
