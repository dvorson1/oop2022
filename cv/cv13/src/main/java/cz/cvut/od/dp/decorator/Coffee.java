package cz.cvut.od.dp.decorator;

public interface Coffee {

    void prepareCoffee();
}
