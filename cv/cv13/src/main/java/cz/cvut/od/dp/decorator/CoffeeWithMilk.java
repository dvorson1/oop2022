package cz.cvut.od.dp.decorator;

public class CoffeeWithMilk extends CoffeeDecorator {

    public CoffeeWithMilk(Coffee coffee) {
        super(coffee);
    }

    @Override
    public void prepareCoffee() {
        super.prepareCoffee();
        System.out.print(" s mlékem");
    }
}
