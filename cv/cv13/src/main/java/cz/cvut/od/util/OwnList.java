package cz.cvut.od.util;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class OwnList<TYPE> implements Iterable<TYPE> {

    private final List<TYPE> backingList = new ArrayList<>();

    public void add(TYPE item) {
        backingList.add(item);
    }

    @Override
    public Iterator<TYPE> iterator() {
        return new OwnIterator<>(backingList);
    }
}
