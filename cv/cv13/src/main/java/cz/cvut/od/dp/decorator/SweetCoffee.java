package cz.cvut.od.dp.decorator;

public class SweetCoffee extends CoffeeDecorator {

    public SweetCoffee(Coffee coffee) {
        super(coffee);
    }

    @Override
    public void prepareCoffee() {
        System.out.print("sladká ");
        super.prepareCoffee();
    }
}
