package cz.cvut.od.dp.command;

public class PrintHelloNameCommand implements GenericCommand {

    private final String name;

    public PrintHelloNameCommand(String name) {
        this.name = name;
    }

    @Override
    public void execute() {
        System.out.println("Hello " + name);
    }
}
