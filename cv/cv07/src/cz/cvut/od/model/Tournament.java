package cz.cvut.od.model;

import java.util.ArrayList;

public class Tournament {
    private String name;
    private ArrayList<Team> teams = new ArrayList<>();
    private ArrayList<Match> matches = new ArrayList<>();

    public void generateMatches() {
        for (int i = 0; i < teams.size(); i++) {
            for (int j = i + 1; j < teams.size(); j++) {
                this.matches.add(new Match(this.teams.get(i), this.teams.get(j)));
            }
        }

        //A B C D
        //AB AC AD
        //BC BD
        // CD
    }

    public void registerTeam(Team team) {
        this.teams.add(team);
    }

    public ArrayList<Match> getMatches() {
        return matches;
    }

    public void setMatches(ArrayList<Match> matches) {
        this.matches = matches;
    }

    public ArrayList<Team> getTeams() {
        return teams;
    }

    public void setTeams(ArrayList<Team> teams) {
        this.teams = teams;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
