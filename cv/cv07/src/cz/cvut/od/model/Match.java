package cz.cvut.od.model;

import java.util.Objects;

public class Match {
    private Team homeTeam;
    private Team visitorTeam;
    private String score;

    public Match(Team homeTeam, Team visitorTeam) {
        this.homeTeam = homeTeam;
        this.visitorTeam = visitorTeam;
    }

    public Team getHomeTeam() {
        return homeTeam;
    }

    public Team getVisitorTeam() {
        return visitorTeam;
    }

    public String getScore() {
        return score;
    }

    public void setScore(String score) {
        this.score = score;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Match match = (Match) o;
        return Objects.equals(homeTeam, match.homeTeam) && Objects.equals(visitorTeam, match.visitorTeam) && Objects.equals(score, match.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(homeTeam, visitorTeam, score);
    }

    @Override
    public String toString() {
        return "Match{" +
                "homeTeam=" + homeTeam +
                ", visitorTeam=" + visitorTeam +
                ", score='" + score + '\'' +
                '}';
    }
}
