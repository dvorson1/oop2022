package cz.cvut.od.model;

public enum CarManufacturer {
    SKODA,
    AUDI,
    BMW
}
