package cz.cvut.od.model;

public class Car {
    private CarManufacturer manufacturer;

    public Car(CarManufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }

    public CarManufacturer getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(CarManufacturer manufacturer) {
        this.manufacturer = manufacturer;
    }
}
