package cz.cvut.od.model;

public abstract class AbstractAnimal implements Named {

    private String name;

    public abstract void makeSound();

    public void jumpAndMakeSound() {
        System.out.println("JUMPED");
        makeSound();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }
}
