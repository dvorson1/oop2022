package cz.cvut.od.model;

public class Duck extends AbstractAnimal {

    @Override
    public void makeSound() {
        System.out.println("QUACK");
    }
}
