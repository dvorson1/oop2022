package cz.cvut.od.model;

public class Cat extends Animal {

    public Cat() {
    }

    public Cat(String name) {
        super(name);
    }

    @Override
    public void makeSound() {
        System.out.println("MNAU");
    }

    public void jump() {
        System.out.println("CAT JUMPED");
    }
}
