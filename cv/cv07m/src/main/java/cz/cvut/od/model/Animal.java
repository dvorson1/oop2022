package cz.cvut.od.model;

public class Animal implements Named{
    private String name;

    public Animal() {
    }

    public Animal(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void makeSound() {
        System.out.println("ANIMAL MADE A SOUND...");
    }
}
