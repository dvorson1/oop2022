package cz.cvut.od.model;

public interface Named {

    public String getName();
    public void setName(String name);
}
