package cz.cvut.od;

import cz.cvut.od.model.AbstractAnimal;
import cz.cvut.od.model.Animal;
import cz.cvut.od.model.Car;
import cz.cvut.od.model.CarManufacturer;
import cz.cvut.od.model.Cat;
import cz.cvut.od.model.Dog;
import cz.cvut.od.model.Duck;
import cz.cvut.od.model.Named;
import cz.cvut.od.model.Person;

import java.util.ArrayList;

public class Playground {

    public void play() {
        //inheritance();
        //interfaces();
        //abstractClass();
    }

    public void enumeration(){
        Car car = new Car(CarManufacturer.AUDI);

        Car car2 = new Car(CarManufacturer.BMW);

        Car car3 = new Car(CarManufacturer.SKODA);

        //new Car("nic")
    }

    public void abstractClass(){
        AbstractAnimal duck = new Duck();
        duck.jumpAndMakeSound();

    }

    public void interfaces(){
        Named animal = new Animal();
        Named cat = new Cat("Tom");
        Named dog = new Dog();
        dog.setName("kkk");
        Named person = new Person();
        person.setName("aaaa");

        ArrayList<Named> namedList = new ArrayList<>();
        namedList.add(animal);
        namedList.add(cat);
        namedList.add(dog);
        namedList.add(person);
        for(Named named: namedList){
            System.out.println(named.getName());
        }
    }

    public void inheritance() {
        Animal animal = new Animal();
        animal.setName("Jerry");

        Animal animal2 = new Animal();
        animal2.setName("Tom");
        animal2.makeSound();

        Cat cat = new Cat();
        cat.setName("Tom");
        cat.makeSound();
        cat.jump();

        Dog dog = new Dog();
        dog.setName("Alik");
        dog.makeSound();

        makeSound(dog);
        makeSound(animal);
        makeSound(cat);

        Animal cat2 = new Cat();
        //tady cat2.jump() nejde
        //pretypovani
        Cat cat3 = (Cat) cat2;
        cat3.jump();
    }

    public void makeSound(Animal a) {
        //check ze realna instance je daneho typu
        if (a instanceof Cat) {
            Cat cat = (Cat) a;
            cat.jump();
        }
        a.makeSound();
    }

}
