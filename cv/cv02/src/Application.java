import java.util.Scanner;

public class Application {

    public static void main(String[] args) {
        //examples();
        System.out.println("VITEJTE!!!");
        Scanner scanner = new Scanner(System.in);
        System.out.print("ZADEJTE PRVNI HODNOTU SOUCTU: ");
        String line = scanner.nextLine();
        System.out.print("ZADEJTE DRUHOU HODNOTU SOUCTU: ");
        System.out.println("VYSLEDEK: " + line);

        //vypiste nejakou uvitaci hlasku
        //vyzvete uzivatele k zadani 1 argumentu (vstupu)
        //vyzvete uzivatele k zadani 2 argumentu (vstupu)
        //vytisknete vysledek ve formatu "VYSLEDEK: XXX"
    }

    //static NAVRATOVY_DAT_TYP NAZEV_METODY(0..N VSTUPU - D_TYP NAZEV_PROMENNE)

    static void examples() {
        //datovy typ nazev_promenne = hodnota_z_oboru_hodnot;
        //celociselne typy (se kterymy budeme pracovat nejvice)
        // int, long
        //textovy datovy typ (String)
        //+ boolean
        //System.out.println("HELLO WORLD");
        printHelloWorld();
        int x = 100;
        int y = 200;
        int z = x + y;
        System.out.println(z);

        printHelloWorld();
        String txtNum = "2";
        int num = 2;
        System.out.println(txtNum + num);

        methodWithInputs("12");
        methodWithMultipleInputs("1", "2", 3);

        int localBlock = 1;
        codeBlockVisibility(localBlock);
        System.out.println(x);

        System.out.println(helloToName("Monika"));

        String helloMonika = helloToName("Monika");
        System.out.println(helloMonika);

        int p, q, r = 1;
        System.out.println("RESULT:");
        System.out.println(sum(100, 1));
    }

    static int sum(int a, int b) {
        return a + b;
    }

    static String helloToName(String name) {
        return "HELLO " + name;
    }

    static void codeBlockVisibility(int input) {
        System.out.println(input);
    }

    static void methodWithMultipleInputs(String input, String input2, int i) {
        System.out.println(input);
        System.out.println(input2);
    }

    static void methodWithInputs(String input) {
        System.out.println(input);
    }

    static void printHelloWorld() {
        System.out.println("HELLO WORLD");
    }
}
