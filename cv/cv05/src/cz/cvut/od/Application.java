package cz.cvut.od;

import cz.cvut.od.model.Illness;
import cz.cvut.od.model.Patient;

public class Application {

    public static void main(String[] args) {
        Patient alois = new Patient();
        alois.setFirstName("Alois");
        alois.setLastName("Dvorak");

        Patient david = new Patient("David");

        Illness flu = new Illness();
        flu.setName("chripka");
        flu.setDeadly(true);

        alois.setIllness(flu);
        System.out.println("Smrtelna: " + alois.isGoingToDie());
        //vytvorte instanci tridy Illness
        //nastavit jeji 2 vlastnosti
        //pacient by mel mit tuto nemoc nastavenou
        //vytisknete vysledek metody isGoingToDie
        System.out.println();
    }

    public void testPrimitiveValues() {
        int count = 0;
        //Integer countI = 0;
        String text = null;
    }
}
