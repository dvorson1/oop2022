package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.List;

public class Patient {

    //atributy, instanční proměnné - vlastnosti dané třídy, nastavujeme/vystavujeme pres get/set metody
    private List list = new ArrayList<>();
    private String personalNumber;
    private String firstName;
    private String lastName;
    private int age;
    private String sex;
    private Illness illness;

    public Patient() {
        System.out.println("byl zalozen novy pacient");
    }
    //konstruktor - volany napr. pri 'new Patient("Adam")';
    public Patient(String firstName){
        this.firstName = firstName;
    }

    public Patient(List list, String personalNumber, String firstName, String lastName, int age, String sex, Illness illness) {
        this.list = list;
        this.personalNumber = personalNumber;
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.sex = sex;
        this.illness = illness;
    }

    public boolean isGoingToDie() {
        if (illness == null) {
            return false;
        }
        return illness.isDeadly();
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public List getList() {
        return list;
    }

    public void setList(List list) {
        this.list = list;
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Illness getIllness() {
        return illness;
    }

    public void setIllness(Illness illness) {
        this.illness = illness;
    }
}
