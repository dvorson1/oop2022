package cz.cvut.od.model.builder;

public class Person {

    private String firstName;
    private String lastName;
    private String age;
    private String placeOfBirth;
    private String address;

    public Person(String firstName, String lastName, String age, String placeOfBirth, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.placeOfBirth = placeOfBirth;
        this.address = address;
    }

    public Person(String firstName, String lastName, String address) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }


    public Person() {
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
