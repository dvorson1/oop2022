package cz.cvut.od.model.builder;

public class BetterPerson {

    private String firstName;
    private String lastName;
    private String age;
    private String placeOfBirth;
    private String address;

    public static BetterPersonBuilder builder() {
        return new BetterPersonBuilder();
    }

    private BetterPerson(BetterPersonBuilder builder) {
        this.firstName = builder.firstName;
        this.lastName = builder.lastName;
        this.age = builder.age;
        this.placeOfBirth = builder.placeOfBirth;
        this.address = builder.address;
    }

    public static class BetterPersonBuilder {
        private String firstName;
        private String lastName;
        private String age;
        private String placeOfBirth;
        private String address;

        public BetterPerson build() {
            return new BetterPerson(this);
        }

        public BetterPersonBuilder setFirstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public BetterPersonBuilder setLastName(String lastName) {
            this.lastName = lastName;
            return this;
        }

        public BetterPersonBuilder setAge(String age) {
            this.age = age;
            return this;
        }

        public BetterPersonBuilder setPlaceOfBirth(String placeOfBirth) {
            this.placeOfBirth = placeOfBirth;
            return this;
        }

        public BetterPersonBuilder setAddress(String address) {
            this.address = address;
            return this;
        }
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getPlaceOfBirth() {
        return placeOfBirth;
    }

    public void setPlaceOfBirth(String placeOfBirth) {
        this.placeOfBirth = placeOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
}
