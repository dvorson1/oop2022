package cz.cvut.od.model.flyweight;

public class Car {
    private CarType type;
    private String color;

    public Car(CarType type, String color) {
        this.type = type;
        this.color = color;
    }
}
