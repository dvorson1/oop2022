package cz.cvut.od.model.singleton;

public class MultiThreadedCommandLineUi {

    private MultiThreadedCommandLineUi(){
        System.out.println("MT singleton created");
    }

    private static class MultiThreadedCommandLineUiHolder {
        private static MultiThreadedCommandLineUi instance = new MultiThreadedCommandLineUi();
    }

    public static MultiThreadedCommandLineUi getInstance() {
        return MultiThreadedCommandLineUiHolder.instance;
    }
}
