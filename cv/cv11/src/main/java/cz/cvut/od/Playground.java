package cz.cvut.od;

import cz.cvut.od.model.builder.BetterPerson;
import cz.cvut.od.model.builder.Person;
import cz.cvut.od.model.flyweight.Car;
import cz.cvut.od.model.flyweight.CarType;
import cz.cvut.od.model.singleton.MultiThreadedCommandLineUi;
import cz.cvut.od.model.singleton.SingleThreadedCommandLineUi;

public class Playground {

    public void play() {
        singleThreadedSingleton();
        multiThreadedSingleton();
    }

    public void flyweightGta(){
        CarType simpleCar = new CarType("personal", "1t", "4");
        CarType truck = new CarType("truck", "10t", "6");

        Car car1 = new Car(simpleCar, "blue");
        Car car2 = new Car(simpleCar, "red");
        Car car3 = new Car(simpleCar, "green");
        Car car4 = new Car(simpleCar, "black");
        Car car5 = new Car(simpleCar, "blue");
        Car car6 = new Car(simpleCar, "blue");
    }

    public void personBuilderProps() {
        Person person = new Person();
        person.setAge("1");
        person.setFirstName("Adam");
        person.setFirstName("Vobornik");

        Person person2 = new Person("Adam", "Vobornik", "1", "Praha", "asdasda");
        Person person3 = new Person("Adam", "Vobornik", "1");

        BetterPerson betterPerson = BetterPerson.builder()
                .setAddress("adresa")
                .setLastName("Vobodnik")
                .setFirstName("Adam")
                .build();

        //rozepsane
        BetterPerson.BetterPersonBuilder betterPersonBuilder = BetterPerson.builder();
        betterPersonBuilder = betterPersonBuilder.setAddress("A");
        betterPersonBuilder = betterPersonBuilder.setPlaceOfBirth("sadasd");

    }

    public void immutableClassExample() {
        String text = "";
        String text2 = new String("");
        String text3 = "A" + "B";
        new StringBuilder("A").append("B").append("C");
    }

    public void singleThreadedSingleton() {
        SingleThreadedCommandLineUi st = SingleThreadedCommandLineUi.getInstance();
        SingleThreadedCommandLineUi st2 = SingleThreadedCommandLineUi.getInstance();
    }

    public void multiThreadedSingleton() {
        MultiThreadedCommandLineUi mt = MultiThreadedCommandLineUi.getInstance();
        MultiThreadedCommandLineUi mt2 = MultiThreadedCommandLineUi.getInstance();
    }

}
