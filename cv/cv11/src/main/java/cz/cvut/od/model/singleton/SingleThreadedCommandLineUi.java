package cz.cvut.od.model.singleton;

public class SingleThreadedCommandLineUi {
    private static SingleThreadedCommandLineUi instance;

    private SingleThreadedCommandLineUi() {
        System.out.println("ST singleton created");
    }

    public static SingleThreadedCommandLineUi getInstance(){
        if(instance == null){
            instance = new SingleThreadedCommandLineUi();
        }
        return instance;
    }

}
