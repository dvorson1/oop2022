package cz.cvut.od.fm;

import cz.cvut.od.model.Polygon;
import cz.cvut.od.model.Square;

public class SquareFactoryMethod extends PolygonFactoryMethod{
    @Override
    public Polygon createPolygon() {
        return new Square();
    }
}
