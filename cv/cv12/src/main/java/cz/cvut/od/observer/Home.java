package cz.cvut.od.observer;

import java.util.ArrayList;
import java.util.List;

public class Home {

    private List<Owner> owners = new ArrayList<>();

    public void doorOpened(){
        System.out.println("DOOR OPENED");
        for (Owner owner: owners){
            owner.doorOpened();
        }
    }

    public List<Owner> getOwners() {
        return owners;
    }
}
