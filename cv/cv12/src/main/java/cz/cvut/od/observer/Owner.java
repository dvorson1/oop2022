package cz.cvut.od.observer;

public class Owner implements Subscriber{

    public void doorOpened() {
        System.out.println("OWNER notified about door being opened..");
    }

    @Override
    public void onAction() {
        doorOpened();
    }
}
