package cz.cvut.od.proxy;

public class RealGate implements Gate {
    @Override
    public Person open(Person p) {
        System.out.println("GATE OPENED");
        return p;
    }
}
