package cz.cvut.od;

import cz.cvut.od.factory.PolygonFactory;
import cz.cvut.od.fm.PolygonFactoryMethod;
import cz.cvut.od.fm.SquareFactoryMethod;
import cz.cvut.od.fm.TriangleFactoryMethod;
import cz.cvut.od.observer.Home;
import cz.cvut.od.observer.ObsHome;
import cz.cvut.od.observer.Owner;
import cz.cvut.od.proxy.Gate;
import cz.cvut.od.proxy.LoggingGate;
import cz.cvut.od.proxy.Person;
import cz.cvut.od.proxy.RealGate;

public class Playground {

    public void play() {
        this.fm();
        this.factory();
        this.proxy();
        this.simpleObserver();
    }

    public void simpleObserver() {
        Home home = new Home();
        home.getOwners().add(new Owner());
        home.getOwners().add(new Owner());
        home.doorOpened();

        ObsHome obsHome = new ObsHome();
        obsHome.getSubscribers().add(new Owner());
        obsHome.getSubscribers().add(new Owner());
        obsHome.doorOpened();
    }

    public void proxy() {
        Gate gate = new RealGate();
        gate.open(new Person());

        gate = new LoggingGate(new RealGate());
        gate.open(new Person());
    }

    public void factory() {
        System.out.println(PolygonFactory.createPolygon(3));
    }

    public void fm() {
        PolygonFactoryMethod squareFactoryMethod = new SquareFactoryMethod();
        squareFactoryMethod.renderPolygon();

        PolygonFactoryMethod triangleFactoryMethod = new TriangleFactoryMethod();
        triangleFactoryMethod.renderPolygon();
    }

}
