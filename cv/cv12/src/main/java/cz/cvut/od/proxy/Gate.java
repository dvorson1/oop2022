package cz.cvut.od.proxy;

public interface Gate {

    Person open(Person p);
}
