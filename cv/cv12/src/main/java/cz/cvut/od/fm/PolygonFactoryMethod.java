package cz.cvut.od.fm;

import cz.cvut.od.model.Polygon;

public abstract class PolygonFactoryMethod {

    public abstract Polygon createPolygon();

    public void renderPolygon() {
        System.out.println(this.createPolygon());
    }
}
