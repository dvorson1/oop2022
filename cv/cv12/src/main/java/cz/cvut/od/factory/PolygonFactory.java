package cz.cvut.od.factory;

import cz.cvut.od.model.Polygon;
import cz.cvut.od.model.Square;
import cz.cvut.od.model.Triangle;

public class PolygonFactory {

    public static Polygon createPolygon(int numSides) {
        if (numSides == 3) {
            return new Triangle();
        } else if (numSides == 4) {
            return new Square();
        } else {
            throw new IllegalArgumentException("cannot handle numSides: " + numSides);
        }
    }
}
