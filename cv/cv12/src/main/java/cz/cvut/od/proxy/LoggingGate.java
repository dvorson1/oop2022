package cz.cvut.od.proxy;

public class LoggingGate implements Gate {

    private Gate realGate;

    public LoggingGate(Gate realGate) {
        this.realGate = realGate;
    }

    @Override
    public Person open(Person p) {
        System.out.println("A TRY TO OPEN A GATE BY: " + p);
        Person returnedPerson = this.realGate.open(p);
        System.out.println("A TRY TO OPEN A GATE FINISHED BY: " + p);
        return returnedPerson;
    }
}
