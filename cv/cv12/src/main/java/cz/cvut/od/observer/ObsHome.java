package cz.cvut.od.observer;

public class ObsHome extends Publisher {

    public void doorOpened() {
        this.sendActionToAll();
    }
}
