package cz.cvut.od.observer;

import java.util.ArrayList;
import java.util.List;

public class Publisher {
    private final List<Subscriber> subscribers = new ArrayList<>();


    public void sendActionToAll() {
        for (Subscriber subscriber : subscribers) {
            subscriber.onAction();
        }
    }

    public List<Subscriber> getSubscribers() {
        return subscribers;
    }
}
