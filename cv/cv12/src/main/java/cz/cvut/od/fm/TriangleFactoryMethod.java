package cz.cvut.od.fm;

import cz.cvut.od.model.Polygon;
import cz.cvut.od.model.Square;
import cz.cvut.od.model.Triangle;

public class TriangleFactoryMethod extends PolygonFactoryMethod{
    @Override
    public Polygon createPolygon() {
        return new Triangle();
    }
}
