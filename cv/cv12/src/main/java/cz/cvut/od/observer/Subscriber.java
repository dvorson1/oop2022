package cz.cvut.od.observer;

public interface Subscriber {

    void onAction();
}
