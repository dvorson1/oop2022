package cz.cvut.od.model;

public class Championship {
    private MatchGenerator matchGenerator;
    private int numMatches;

    public Championship(MatchGenerator matchGenerator) {
        this.matchGenerator = matchGenerator;
    }

    public int generateMatches() {
        this.numMatches = matchGenerator.generateMatches();
        if (this.numMatches < 1) {
            throw new IllegalArgumentException("error");
        }
        return this.numMatches;
    }
}
