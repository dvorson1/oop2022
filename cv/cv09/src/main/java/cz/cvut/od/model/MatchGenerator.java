package cz.cvut.od.model;

public interface MatchGenerator {

    int generateMatches();
}
