package cz.cvut.od.model;

public class Calculator {

    private ILongRunningOperationsCalculator longRunningOperationsCalculator;

    public Calculator(ILongRunningOperationsCalculator longRunningOperationsCalculator) {
        this.longRunningOperationsCalculator = longRunningOperationsCalculator;
    }

    public int calculateStuff(int i, int j) {

        int result = longRunningOperationsCalculator.performLongRunningOperation(i, j);
        if (result < 0) {
            throw new IllegalArgumentException("result from long running op. is < 0");
        }
        return result + 1;
    }
}
