package cz.cvut.od.model;

public interface ILongRunningOperationsCalculator {

    int performLongRunningOperation(int i, int j);

}
