package cz.cvut.od.model;

public class MockLongRunningOperationsCalculator implements ILongRunningOperationsCalculator {
    @Override
    public int performLongRunningOperation(int i, int j) {
        return -1;
    }
}
