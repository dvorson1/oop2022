package cz.cvut.od.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

public class ChampionshipTest {

    @Test
    public void generateMatches_invalidNumberOfMatches_throwException() {
        MatchGenerator matchGenerator = Mockito.mock(MatchGenerator.class);
        Mockito.when(matchGenerator.generateMatches()).thenReturn(-1);
        Championship championship = new Championship(matchGenerator);
        Assertions.assertThrows(IllegalArgumentException.class, () -> championship.generateMatches());
    }

    @Test
    public void generateMatches_numberOfMatchesValid_returnThatNumber() {
        MatchGenerator matchGenerator = Mockito.mock(MatchGenerator.class);
        Mockito.when(matchGenerator.generateMatches()).thenReturn(5);
        Championship championship = new Championship(matchGenerator);
        Assertions.assertEquals(5, championship.generateMatches());
    }
}
