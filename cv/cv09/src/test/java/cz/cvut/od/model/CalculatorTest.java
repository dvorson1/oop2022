package cz.cvut.od.model;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;
import java.util.List;

public class CalculatorTest {

    @DisplayName("Test input < 0, throw exception")
    @Test
    public void calculateStuff_resultFromLongRunningOpLessThanZero_throwIllegalArgumentException() {
        Calculator calculator = new Calculator(new MockLongRunningOperationsCalculator());
        Assertions.assertThrows(IllegalArgumentException.class, () -> calculator.calculateStuff(10, 10));
    }

    @DisplayName("Mockito - Test input < 0, throw exception")
    @Test
    public void calculateStuff_resultFromLongRunningOpLessThanZero_throwIllegalArgumentExceptionMockito() {
        ILongRunningOperationsCalculator mock = Mockito.mock(ILongRunningOperationsCalculator.class);
        Mockito.when(mock.performLongRunningOperation(10, 10)).thenReturn(-5);

        Calculator calculator = new Calculator(mock);
        Assertions.assertThrows(IllegalArgumentException.class, () -> calculator.calculateStuff(10, 10));
    }

    @Test
    public void sortTest(){
        List<Integer> list = Arrays.asList(2, 3, 8, 0, 9, -1);

        list.sort((o1, o2) -> o1.compareTo(o2));
        
        System.out.println(list);
    }
}
