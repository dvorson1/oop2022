package cz.cvut.od;

import cz.cvut.od.model.Illness;
import cz.cvut.od.model.Patient;

import java.util.ArrayList;

public class Application {

    public static void main(String[] args) {
        //equalityTests();
        //patientEqualityTests();
        hospitalApp();
    }

    public static void hospitalApp() {
        Patient p1 = new Patient("1", "Franta", "Vomacka");
        p1.addIllness(new Illness("ryma"));
        p1.addIllness(new Illness("kasel"));
        p1.addIllness(new Illness("angina"));

        p1.getIllnessesList().add(new Illness("covid"));

        ArrayList<Illness> illnessesList = p1.getIllnessesList();
        illnessesList.add(new Illness("sars"));

        p1.addIllnessToList(new Illness("1111"));

        for (Illness illness: p1.getIllnessesList()) {
            System.out.println(illness);
        }

        for(int i = 0; i < p1.getIllnessesList().size(); i++){
            System.out.println(p1.getIllnessesList().get(i));
        }

        System.out.println(p1);
    }

    public static void equalityTests() {
        Integer a = 8000;
        Integer b = 8000;

        int c = 8000;
        int d = 8000;

        System.out.println("1. " + (a.equals(b)));
        System.out.println("2. " + (c == d));
    }

    public static void patientEqualityTests() {
        Patient p1 = new Patient("1", "Franta", "Vomacka");
        Patient p2 = new Patient("2", "David", "Mikes");
        Patient p3 = p1;
        Patient p4 = null;

        String a = "1";
        Integer b = 1;

        Object bObj = b;

        a.equals(b);

        System.out.println(p1 == p2);
        System.out.println(p1 == p3);

        System.out.println(p1.equals(p2));
        if (p4 != null) {
            System.out.println(p4.equals(p1));
        }
        System.out.println("AHOJ");
        System.out.println(p1);
    }
}
