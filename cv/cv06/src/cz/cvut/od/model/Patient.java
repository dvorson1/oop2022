package cz.cvut.od.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Objects;

public class Patient {
    private String personalNumber;
    private String firstName;
    private String lastName;

    private Illness[] illnesses = new Illness[50];
    private int currIdx = 0;

    private ArrayList<Illness> illnessesList = new ArrayList<>();

    public Patient() {
    }

    public Patient(String personalNumber, String firstName, String lastName) {
        this.personalNumber = personalNumber;
        this.firstName = firstName;
        this.lastName = lastName;
    }

    @Override
    public String toString() {
        return "Patient{" +
                "personalNumber='" + personalNumber + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", illnesses=" + Arrays.toString(illnesses) +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Patient patient = (Patient) o;
        return Objects.equals(personalNumber, patient.personalNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(personalNumber);
    }

    public String getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(String personalNumber) {
        this.personalNumber = personalNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void addIllness(Illness illness) {
        this.illnesses[currIdx] = illness;
        this.currIdx++;

    }

    public ArrayList<Illness> getIllnessesList() {
        return illnessesList;
    }

    public void addIllnessToList(Illness illness){
        this.illnessesList.add(illness);
    }

    public void setIllnessesList(ArrayList<Illness> illnessesList) {
        this.illnessesList = illnessesList;
    }

    public Illness[] getIllnesses() {
        return illnesses;
    }

    public void setIllnesses(Illness[] illnesses) {
        this.illnesses = illnesses;
    }
}
