package cz.cvut.od.model;

import java.util.Objects;

public class Illness {
    private String name;

    public Illness(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Illness{" +
                "name='" + name + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Illness illness = (Illness) o;
        return Objects.equals(name, illness.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }
}
