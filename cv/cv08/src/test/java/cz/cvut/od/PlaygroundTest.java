package cz.cvut.od;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

public class PlaygroundTest {

    @DisplayName("summing two positive integers results in expected value")
    @Test
    public void sumPositiveTest() {
        Playground playground = new Playground();
        int result = playground.sum(1, 1);
        Assertions.assertEquals(2, result);
    }

    @DisplayName("only even number in the list are summed if no negative is present")
    @Test
    public void sumOnlyEvenNumbersTest() {
        Playground playground = new Playground();
        int result = playground.sum(List.of(2, 2, 3, 5, 4));
        Assertions.assertEquals(8, result);
    }

    @DisplayName("IllegalArgumentException is thrown when negative value is present")
    @Test
    public void sumInputContainsNegativeValueTest() {
        Playground playground = new Playground();
        /*boolean wasThrown = false;
        try {
            playground.sum(List.of(2, 2, 3, -1, 4));
        }catch (IllegalArgumentException e){
            wasThrown = true;
        }
        Assertions.assertTrue(wasThrown);*/
        Assertions.assertThrows(IllegalArgumentException.class, () -> playground.sum(List.of(2, 2, 3, -1, 4)));
    }
}
