package cz.cvut.od;

public interface Calculator {

    int sum(int i, int j);
}
