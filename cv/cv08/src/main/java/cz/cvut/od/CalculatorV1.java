package cz.cvut.od;

public class CalculatorV1 implements Calculator{

    @Override
    public int sum(int i, int j) {
        return i + j;
    }
}
