package cz.cvut.od;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Playground {

    private Calculator calculator = new CalculatorV2();
    private List<String> stringList = new ArrayList<>();

    public void play() {
        ArrayList<Integer> a = new ArrayList<>();
        a.add(1);
        a.add(-2);

        calculator.sum(1, 2);

        Arrays.asList(1, 2, 3, 4, 5);
        List.of(1, 2, 3, 4, 5);
    }

    public int sum(List<Integer> listToSum) {
        int result = 0;
        for (Integer i : listToSum) {
            if (i < 0) {
                throw new IllegalArgumentException("input cannot contain negative numbers");
            }
            if (i % 2 == 0) {
                result += i;
                //result = i + result;
            }
        }
        return result;
    }

    public int sum(int i, int j) {
        return i + j;
    }

    //napiste metodu, co umi scitat pouze suda cisla napric polem (ArrayList)
    //pokud dostanete v poli (ArrayList) zapornou hodnotu, metoda vyhodi IllegalArgumentException
    //vymyslete a napiste smysluplne testy
}
