package cz.cvut.test;

import java.util.Random;

public class Application {

    public static String random4Digits() {
        return String.format("%04d", new Random().nextInt(10000));
    }
}
